package com.noroff.boxinator.models;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Country implements Comparable<Country>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;



    // A country has a foreign key to its region
    @ManyToOne
    @JoinColumn(name="region_id")
    private Region region;

    public Country(String name) {

        this.name = name;
    }

    // A country can 0 or many addresses
    @OneToMany(mappedBy = "country")
    private Set<Address> addresses;
    
    public Country(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    // Method to use with Collections.sort() to sort by id
    @Override
    public int compareTo(Country otherCountry) {
        return this.name.compareTo(otherCountry.getName());
    }
}
