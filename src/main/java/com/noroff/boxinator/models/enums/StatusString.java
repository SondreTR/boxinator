package com.noroff.boxinator.models.enums;

public enum StatusString{
    CREATED,
    RECEIVED,
    INTRANSIT,
    COMPLETED,
    CANCELLED
}
