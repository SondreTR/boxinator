package com.noroff.boxinator.models.enums;

public enum AccountType {
    GUEST,
    REGISTERED_USER,
    ADMINISTRATOR
}
