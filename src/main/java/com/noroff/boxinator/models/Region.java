package com.noroff.boxinator.models;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Region implements Comparable<Region>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "multiplier", nullable = false)
    private double multiplier;

    // A region can have many countries
    @OneToMany(mappedBy = "region")
    Set<Country> countries;

    public Region(String name, double multiplier) {
        this.name = name;
        this.multiplier = multiplier;
    }

    public Region(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(double multiplier) {
        this.multiplier = multiplier;
    }

    // Method to use with Collections.sort() to sort by id
    @Override
    public int compareTo(Region otherRegion) {
        int result = (int)(this.id - otherRegion.getId());
        return result;
    }
}
