package com.noroff.boxinator.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="street", nullable = false)
    private String street;

    @Column(name="city", nullable = false)
    private String city;

    @Column(name="postal_code", nullable = false)
    private String postalCode;

    @Column(name="state")
    private String state;

    // An address must have one country
    @ManyToOne()
    @JoinColumn(name="country_id")
    private Country country;

    // One address can have many shipments
    @JsonIgnore
    @OneToMany(mappedBy = "address")
    Set<Shipment> shipments;

//    @JsonGetter("shipments")
//    public List<String> getAllShipments() {
//        if (shipments != null){
//            return shipments.stream()
//                    .map(shipment -> {
//                        return "Id: " + shipment.getId();
//                    }).collect(Collectors.toList());
//        }
//        return null;
//    }

    // One address can have one or more profiles
    @JsonIgnore
    @OneToMany(mappedBy = "address")
    Set<Profile> profiles;

//    @JsonGetter("profiles")
//    public List<String> getAllProfiles() {
//        if (profiles != null){
//            return profiles.stream()
//                    .map(profile -> {
//                        return "id: " + profile.getId();
//                    }).collect(Collectors.toList());
//        }
//        return null;
//    }

    public Address() {}

    public Address(String street, String city, String postalCode, String state, Country country) {
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
        this.state = state;
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Set<Shipment> getShipments() {
        return shipments;
    }

    public void setShipments(Set<Shipment> shipments) {
        this.shipments = shipments;
    }

    public Set<Profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(Set<Profile> profiles) {
        this.profiles = profiles;
    }
}
