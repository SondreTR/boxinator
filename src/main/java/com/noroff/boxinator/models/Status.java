package com.noroff.boxinator.models;

import com.noroff.boxinator.models.enums.StatusString;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Status {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", nullable = false)
    private long id;

    @Column(name="status", nullable = false)
    private StatusString status;

    @OneToMany(mappedBy = "status")
    private Set<ShipmentStatus> shipmentStatuses;

    public Status(StatusString status) {
        this.status = status;
    }

    public Status() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public StatusString getStatus() {
        return status;
    }

    public void setStatus(StatusString status) {
        this.status = status;
    }
}
