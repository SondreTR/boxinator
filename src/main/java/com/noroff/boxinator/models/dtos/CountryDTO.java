package com.noroff.boxinator.models.dtos;

public class CountryDTO {
    private String countryName;
    private String regionName;

    public CountryDTO(String countryName, String regionName) {
        this.countryName = countryName;
        this.regionName = regionName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
}
