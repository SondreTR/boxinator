package com.noroff.boxinator.models.dtos;

import com.noroff.boxinator.models.Package;
import com.noroff.boxinator.models.enums.StatusString;

import java.util.Set;

public class ShipmentDTO {

    private String email;

    private String street;

    private String city;

    private String postalCode;

    private String state;

    private String country;

    private Set<Package> packages;

    // Enum to store status
    private StatusString status;

    private double cost;

    public ShipmentDTO(String email, String street, String city, String postalCode, String state, String country) {
        this.email = email;
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
        this.state = state;
        this.country = country;
    }

    public Set<Package> getPackages() {
        return packages;
    }

    public void setPackages(Set<Package> packages) {
        this.packages = packages;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public StatusString getStatus() {
        return status;
    }

    public void setStatus(StatusString status) {
        this.status = status;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
