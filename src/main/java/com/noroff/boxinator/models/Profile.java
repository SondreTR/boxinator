package com.noroff.boxinator.models;

import com.noroff.boxinator.models.enums.AccountType;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Profile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;

    // A guest user should be able to register with only an email, so this must be unique
    @Column(name="email", nullable = false, unique = true)
    private String email;

    @Column(name="date_of_birth")
    private String dateOfBirth;

    @Column(name="contact_number")
    private String contactNumber;

    @Column(name="account_type")
    private AccountType accountType;

    // Store key from keycloak database
    @Column(name="keycloak_id")
    private String keycloakId;

    // A profile must have one address
    @ManyToOne
    @JoinColumn(name = "address_id")
    private Address address;

    // A profile can have 0 or many shipments
    @OneToMany(mappedBy="profile", fetch = FetchType.EAGER)
    private Set<Shipment> shipments;

    public Profile(String firstName, String lastName, String email, String dateOfBirth, String contactNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.contactNumber = contactNumber;
    }

    public Profile() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Set<Shipment> getShipments() {
        return shipments;
    }

    public void setShipments(Set<Shipment> shipments) {
        this.shipments = shipments;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public String getKeycloakId() {
        return keycloakId;
    }

    public void setKeycloakId(String keycloakId) {
        this.keycloakId = keycloakId;
    }
}
