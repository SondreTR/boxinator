package com.noroff.boxinator.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
public class Shipment implements Comparable<Shipment>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="email", nullable = false)
    private String email;

    // A shipment must have one address
    @ManyToOne
    @JoinColumn(name = "address_id")
    private Address address;

    // A shipment must have one profile
    @ManyToOne
    @JoinColumn(name = "profile_id")
    private Profile profile;

    @JsonGetter("profile")
    public String profile() {
        if (profile != null)
            return "" + profile.getId();
        else
            return null;
    }

    @OneToMany(mappedBy = "shipment")
    private Set<Package> packages;

    @OneToMany(mappedBy = "shipment")
    private Set<ShipmentStatus> shipmentStatuses;

    @JsonGetter("shipmentStatuses")
    public LinkedHashSet<ShipmentStatus> shipmentStatuses(){
        // Sort Shipment statuses by earliest to latest date
        if (shipmentStatuses != null){
            ArrayList<ShipmentStatus> list = new ArrayList<>(shipmentStatuses);
            Collections.sort(list);
            LinkedHashSet<ShipmentStatus> set = new LinkedHashSet<>(list);
            return set;
        }
        return null;
    }

    // Total cost of shipment. Calculated by taking all packages' weight multiplied
    // by country multiplier + 200
    @JoinColumn(name = "cost")
    private double cost;

    public Shipment(String email) {
        this.email = email;
    }

    public Shipment() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Set<Package> getPackages() {
        return packages;
    }

    public void setPackages(Set<Package> packages) {
        this.packages = packages;
    }

    public Set<ShipmentStatus> getShipmentStatuses() {
        return shipmentStatuses;
    }

    public void setShipmentStatuses(Set<ShipmentStatus> shipmentStatuses) {
        this.shipmentStatuses = shipmentStatuses;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    // Make objects sortable by date from earliest to latest
    @Override
    public int compareTo(Shipment other) {
        return Integer.parseInt((other.id - this.id) + "");
    }
}
