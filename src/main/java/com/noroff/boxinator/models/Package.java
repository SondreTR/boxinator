package com.noroff.boxinator.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Package {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name="type", nullable = false)
    private String type;

    @Column(name="weight", nullable = false)
    private String weight;

    @Column(name="color", nullable = false)
    private String color;

    @ManyToOne
    @JoinColumn(name = "shipment_id")
    private Shipment shipment;

    @JsonGetter("shipment")
    public String shipment() {
        if (shipment != null)
            return "" + shipment.getId();
        else
            return null;
    }

    public Package(String type, String weight, String color) {
        this.type = type;
        this.weight = weight;
        this.color = color;
    }

    public Package() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Shipment getShipment() {
        return shipment;
    }

    public void setShipment(Shipment shipment) {
        this.shipment = shipment;
    }
}
