package com.noroff.boxinator.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import javax.persistence.*;

@Entity
public class ShipmentStatus implements Comparable<ShipmentStatus> {

    @Id
    @Column(name="date", nullable = false)
    private String date;

    @ManyToOne
    @JoinColumn(name = "shipment_id")
    private Shipment shipment;

    @JsonGetter("shipment")
    public String shipment() {
        if (shipment != null)
            return "" + shipment.getId();
        else
            return null;
    }

    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    public ShipmentStatus(String date, Shipment shipment, Status status) {
        this.date = date;
        this.shipment = shipment;
        this.status = status;
    }

    public ShipmentStatus() {}


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Shipment getShipment() {
        return shipment;
    }

    public void setShipment(Shipment shipment) {
        this.shipment = shipment;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    // Make objects sortable by date from earliest to latest
    @Override
    public int compareTo(ShipmentStatus other) {
        return this.getDate().compareTo(other.getDate());
    }
}
