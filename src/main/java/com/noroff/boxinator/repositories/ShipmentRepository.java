package com.noroff.boxinator.repositories;

import com.noroff.boxinator.models.Shipment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShipmentRepository extends JpaRepository<Shipment, Long> {

    Shipment findById(long id);
}
