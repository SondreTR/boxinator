package com.noroff.boxinator.repositories;

import com.noroff.boxinator.models.Package;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PackageRepository extends JpaRepository<Package, Long> {

    Package findById(long id);
}
