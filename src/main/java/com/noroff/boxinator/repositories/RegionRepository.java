package com.noroff.boxinator.repositories;

import com.noroff.boxinator.models.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegionRepository extends JpaRepository<Region, Long> {

    Region findById(long id);

    boolean existsRegionByName(String regionName);

    Region findByName(String regionName);
}
