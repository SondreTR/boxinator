package com.noroff.boxinator.repositories;

import com.noroff.boxinator.models.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
    Country findById(long id);

    boolean existsCountryByName(String name);

    Country findByName(String name);


}
