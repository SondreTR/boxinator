package com.noroff.boxinator.repositories;

import com.noroff.boxinator.models.ShipmentStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// TODO - the primary key here is the the combination of foreign keys of shipment and status
@Repository
public interface ShipmentStatusRepository extends JpaRepository<ShipmentStatus, String> {
}
