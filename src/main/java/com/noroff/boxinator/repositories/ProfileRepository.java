package com.noroff.boxinator.repositories;

import com.noroff.boxinator.models.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {

    Profile findById(long id);
    boolean existsById(long id);
    boolean existsByEmail(String email);
    Profile findByKeycloakId(String keycloakId);
    Profile findByEmail(String email);
}
