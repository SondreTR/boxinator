package com.noroff.boxinator.controllers;

import com.noroff.boxinator.models.Address;
import com.noroff.boxinator.models.Country;
import com.noroff.boxinator.models.Profile;
import com.noroff.boxinator.models.Shipment;
import com.noroff.boxinator.models.dtos.ProfileDTO;
import com.noroff.boxinator.services.AddressService;
import com.noroff.boxinator.services.CountryService;
import com.noroff.boxinator.services.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.security.Principal;
import java.util.List;

@RestController
@CrossOrigin(origins = "https://lit-spire-70256.herokuapp.com/")
@RequestMapping("/api/profiles")
public class ProfileController {

    @Autowired
    ProfileService profileService;

    @Autowired
    AddressService addressService;

    @Autowired
    CountryService countryService;

    private HttpStatus status;

    // Return all profiles
    @GetMapping()
    public ResponseEntity<List<Profile>> getProfiles(){
        List<Profile> profiles = profileService.getProfiles();
        status = HttpStatus.OK;
        return new ResponseEntity<>(profiles, status);
    }

    // Get currently logged in user
    @GetMapping("/user")
    public ResponseEntity<Profile> getProfile(Principal principal){
        String keycloakId = principal.getName();
        Profile profile = profileService.findByKeycloakId(keycloakId);
        status = HttpStatus.OK;
        if (profile == null) status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(profile, status);
    }

    // Return given character by id
    @GetMapping("/{id}")
    public ResponseEntity<Profile> getProfileById(@PathVariable Long id) {
        Profile profile = new Profile();
        // Check first if Character exists
        if (profileService.existsById(id)) {
            status = HttpStatus.OK;
            profile = profileService.getProfileById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(profile, status);
    }

    @GetMapping("/registration/{email}")
    public ResponseEntity<Profile> getProfileByEmail(@PathVariable String email) {
        Profile returnProfile = profileService.findProfileByEmail(email);
        status = HttpStatus.OK;
        if (returnProfile == null ) status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(returnProfile, status);
    }

    // Post new profile to database
    @PostMapping
    public ResponseEntity<Profile> addProfile(@RequestBody ProfileDTO profileDTO){
        Profile returnProfile = profileService.createProfileWithAddress(profileDTO);
        status = HttpStatus.CREATED;
        // If email is already taken
        if (returnProfile == null) status = HttpStatus.CONFLICT;
        return new ResponseEntity<>(returnProfile, status);
    }

    // Update profile in database
    @PutMapping("{id}")
    public ResponseEntity<Profile> updateProfile(@PathVariable long id, @RequestBody ProfileDTO profileDTO){
        Profile returnProfile = profileService.updateProfile(id, profileDTO);
        status = HttpStatus.CREATED;
        if (returnProfile == null) status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(returnProfile, status);
    }

    // Delete a profile and related shipments, packages and statuses. Only accessible by admins
    @DeleteMapping("{id}")
    ResponseEntity<Profile> deleteProfile(@PathVariable long id) {
        // Check if profile exists, then delete if it does
        if (profileService.existsById(id)) {
            status = HttpStatus.OK;
            profileService.deleteByID(id);
        }
        else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(status);
    }

}
