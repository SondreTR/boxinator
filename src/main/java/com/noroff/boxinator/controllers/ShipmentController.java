package com.noroff.boxinator.controllers;

import com.noroff.boxinator.models.*;
import com.noroff.boxinator.models.Package;
import com.noroff.boxinator.models.dtos.CountryDTO;
import com.noroff.boxinator.models.dtos.ShipmentDTO;
import com.noroff.boxinator.models.enums.StatusString;
import com.noroff.boxinator.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@CrossOrigin(origins = "https://lit-spire-70256.herokuapp.com/")
@RequestMapping("/api/shipments")
public class ShipmentController {

    @Autowired
    ShipmentService shipmentService;

    @Autowired
    ProfileService profileService;

    @Autowired
    CountryService countryService;

    @Autowired
    AddressService addressService;

    @Autowired
    PackageService packageService;

    @Autowired
    ShipmentStatusService shipmentStatusService;

    @Autowired
    StatusService statusService;


    private HttpStatus status;

    // Return all shipments
    @GetMapping()
    public ResponseEntity<List<Shipment>> getShipments(){
        List<Shipment> shipments = shipmentService.getShipments();
        Collections.sort(shipments);
        status = HttpStatus.OK;
        return new ResponseEntity<>(shipments, status);
    }

    // Return all completed shipments for logged in user
    @GetMapping("/complete")
    public ResponseEntity<List<Shipment>> getCompletedShipments(Principal principal){
        String keycloakId = principal.getName();
        List<Shipment> shipments = shipmentService.getCompletedShipments(keycloakId);
        Collections.sort(shipments);
        status = HttpStatus.OK;
        return new ResponseEntity<>(shipments, status);
    }

    // Return all cancelled shipments for logged in user
    @GetMapping("/cancelled")
    public ResponseEntity<List<Shipment>> getCancelledShipments(Principal principal){
        String keycloakId = principal.getName();
        List<Shipment> shipments = shipmentService.getCancelledShipments(keycloakId);
        Collections.sort(shipments);
        status = HttpStatus.OK;
        return new ResponseEntity<>(shipments, status);
    }

    // Return given shipment by id
    @GetMapping("/{id}")
    public ResponseEntity<Shipment> getShipment(@PathVariable Long id) {
        Shipment shipment = new Shipment();
        // Check first if Shipment exists
        if (shipmentService.existsById(id)) {
            status = HttpStatus.OK;
            shipment = shipmentService.getShipmentById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(shipment, status);
    }

    @GetMapping("/receipt/{id}")
    public ResponseEntity<Shipment> getShipmentReceipt(@PathVariable Long id) {
        Shipment shipmentReceipt = new Shipment();
        // Check first if Shipment exists
        if (shipmentService.existsById(id)) {
            status = HttpStatus.OK;
            shipmentReceipt = shipmentService.getShipmentReceipt(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(shipmentReceipt, status);
    }

    // Return all shipments for a given user
    @GetMapping("/customer/{id}")
    public ResponseEntity<Set<Shipment>> getProfileShipments(@PathVariable Long id) {
        Set<Shipment> shipments = new HashSet<>();
        Profile profile = new Profile();
        // Check if user exists
        if (profileService.existsById(id)) {
            profile = profileService.getProfileById(id);
            shipments = profile.getShipments();
            status = HttpStatus.OK;
        }
        else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(shipments, status);
    }

    // Post new shipment to database as a registered user. Eventual packages sent with the json-body will be created here as well.
    @PostMapping()
    public ResponseEntity<Shipment> addShipment(@RequestBody ShipmentDTO shipmentDTO, Principal principal){
        Shipment shipment = new Shipment();
        // For registered user
        if (principal != null){
            String keycloakId = principal.getName();
            shipment = shipmentService.createShipment(keycloakId, shipmentDTO);
        }
        else{
            shipment = shipmentService.createGuestShipment(shipmentDTO);
        }
        status = HttpStatus.CREATED;
        if (shipment == null) status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(shipment, status);
    }

    // Method allows admins to update the status of a shipment
    @PutMapping("{id}")
    ResponseEntity<Shipment> updateShipmentStatus(@PathVariable long id, @RequestBody ShipmentDTO shipmentDTO){
        Shipment returnShipment = shipmentService.updateShipmentStatusById(id, shipmentDTO);
        status = HttpStatus.OK;
        if (returnShipment == null) status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(returnShipment, status);
    }

    // Method deletes a shipment and all related statuses and packages
    @DeleteMapping("{id}")
    ResponseEntity<Shipment> deleteShipment(@PathVariable long id) {
        // Check if shipment exists, then delete if it does
        if (shipmentService.existsById(id)) {
            status = HttpStatus.OK;
            shipmentService.deleteByID(id);
        }
        else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(status);
    }




}
