package com.noroff.boxinator.controllers;

import com.noroff.boxinator.models.Address;
import com.noroff.boxinator.models.Package;
import com.noroff.boxinator.models.Profile;
import com.noroff.boxinator.models.Shipment;
import com.noroff.boxinator.models.dtos.ShipmentDTO;
import com.noroff.boxinator.services.PackageService;
import com.noroff.boxinator.services.ShipmentService;
import com.sun.xml.bind.v2.runtime.reflect.Lister;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin(origins = "https://lit-spire-70256.herokuapp.com/")
@RequestMapping("/api/packages")
public class PackageController {

    @Autowired
    PackageService packageService;

    @Autowired
    ShipmentService shipmentService;

    private HttpStatus status;

    // Return all packages
    @GetMapping()
    public ResponseEntity<List<Package>> getPackage(){
        List<Package> packages = packageService.getPackages();
        status = HttpStatus.OK;
        return new ResponseEntity<>(packages, status);
    }

    // Return given package by id
    @GetMapping("/{id}")
    public ResponseEntity<Package> getPackage(@PathVariable Long id) {
        Package p = new Package();
        // Check first if Package exists
        if (packageService.existsById(id)) {
            status = HttpStatus.OK;
            p = packageService.getPackageById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(p, status);
    }

    // Adds a package without a shipment. This method is never used and also futile
    @PostMapping
    public ResponseEntity<Package> addPackage(@RequestBody Package p){
        Package returnPackage = packageService.save(p);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnPackage, status);
    }

    // Adds a package to the shipment with the given id
    @PutMapping("/{id}")
    public ResponseEntity<Package> addPackageToShipment(@PathVariable Long id, @RequestBody Package p){
        Package returnPack = packageService.addPackageToShipment(id, p);
        status = HttpStatus.CREATED;
        if (returnPack == null) status = HttpStatus.CONFLICT;
        return new ResponseEntity<>(returnPack, status);
    }

}
