package com.noroff.boxinator.controllers;

import com.noroff.boxinator.models.Address;
import com.noroff.boxinator.models.Shipment;
import com.noroff.boxinator.models.ShipmentStatus;
import com.noroff.boxinator.services.AddressService;
import com.noroff.boxinator.services.ShipmentStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@CrossOrigin(origins = "https://lit-spire-70256.herokuapp.com/")
@RequestMapping("/api/shipmentstatuses")
public class ShipmentStatusController {

    @Autowired
    ShipmentStatusService shipmentStatusService;

    private HttpStatus status;

    // Return all shipmentStatuses
    @GetMapping()
    public ResponseEntity<List<ShipmentStatus>> getShipmentStatuses(){
        List<ShipmentStatus> shipmentStatuses = shipmentStatusService.getShipmentStatuses();
        Collections.sort(shipmentStatuses);
        status = HttpStatus.OK;
        return new ResponseEntity<>(shipmentStatuses, status);
    }

    @PostMapping
    public ResponseEntity<ShipmentStatus> addShipmentStatus(@RequestBody ShipmentStatus shipmentStatus){
        ShipmentStatus returnShipmentStatus = shipmentStatusService.save(shipmentStatus);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnShipmentStatus, status);
    }
}
