package com.noroff.boxinator.controllers;


import com.noroff.boxinator.models.Country;
import com.noroff.boxinator.models.Profile;
import com.noroff.boxinator.models.Shipment;
import com.noroff.boxinator.models.dtos.CountryDTO;
import com.noroff.boxinator.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.HashSet;
import javax.annotation.security.RolesAllowed;
import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin(origins = "https://lit-spire-70256.herokuapp.com/")
@RequestMapping("/api/settings/countries")
public class CountryController {

    @Autowired
    CountryService countryService;

    private HttpStatus status;

    // Get all countries
    @GetMapping()
    public ResponseEntity<List<Country>> getCountries(){
        List<Country> countries = countryService.getCountries();
        // Sort countries alphabetically
        Collections.sort(countries);
        // Remove Norway, Sweden and Denmark from the list and add them to the start
        for (int i = 0; i < countries.size(); i++) {
            if (countries.get(i).getRegion().getName().equals("Scandinavia")){
                Country scanCountry = countries.remove(i);
                countries.add(0, scanCountry);
            }
        }
        status = HttpStatus.OK;
        return new ResponseEntity<>(countries, status);
    }

    // Get country by id
    @GetMapping("/{id}")
    public ResponseEntity<Country> getCountry(@PathVariable Long id){
        Country country = new Country();

        if(countryService.existsById(id)){
            status = HttpStatus.OK;
            country = countryService.getCountryById(id);
        }
        else status = HttpStatus.NOT_FOUND;

        return new ResponseEntity<>(country, status);
    }

    // Create new country
    @RolesAllowed("admin")
    @PostMapping()
    public ResponseEntity<Country> addCountry(@RequestBody CountryDTO countryDTO){

        Country returnCountry = countryService.createCountryWithRegion(countryDTO);
        status = HttpStatus.CREATED;

        if (returnCountry == null) status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(returnCountry, status);
    }

    // Update country
    @PutMapping("{id}")
    public ResponseEntity<Country> updateCountry(@PathVariable long id, @RequestBody CountryDTO countryDTO){
        Country returnCountry = countryService.updateCountryById(id, countryDTO);
        status = HttpStatus.OK;
        if (returnCountry == null) status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(returnCountry, status);
    }




}
