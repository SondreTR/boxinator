package com.noroff.boxinator.controllers;

import com.noroff.boxinator.models.Address;
import com.noroff.boxinator.models.ShipmentStatus;
import com.noroff.boxinator.models.Status;
import com.noroff.boxinator.services.AddressService;
import com.noroff.boxinator.services.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "https://lit-spire-70256.herokuapp.com/")
@RequestMapping("/api/statuses")
public class StatusController {

    @Autowired
    StatusService statusService;

    private HttpStatus httpStatus;

    // Return all addresses
    @GetMapping()
    public ResponseEntity<List<Status>> getStatuses(){
        List<Status> statuses = statusService.getStatuses();
        httpStatus = HttpStatus.OK;
        return new ResponseEntity<>(statuses, httpStatus);
    }

    @PostMapping
    public ResponseEntity<Status> addShipmentStatus(@RequestBody Status status){
        Status returnShipmentStatus = statusService.save(status);
        httpStatus = HttpStatus.CREATED;
        return new ResponseEntity<>(returnShipmentStatus, httpStatus);
    }

}
