package com.noroff.boxinator.controllers;

import com.noroff.boxinator.models.Country;
import com.noroff.boxinator.models.Profile;
import com.noroff.boxinator.models.Region;
import com.noroff.boxinator.models.Shipment;
import com.noroff.boxinator.models.dtos.CountryDTO;
import com.noroff.boxinator.services.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Role;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.Collections;
import java.util.List;

@RestController
@CrossOrigin(origins = "https://lit-spire-70256.herokuapp.com/")
@RequestMapping("/api/regions")
public class RegionController {

    @Autowired
    RegionService regionService;

    private HttpStatus status;

    // Return all regions
    @GetMapping()
    public ResponseEntity<List<Region>> getRegions(){
        List<Region> regions = regionService.getRegions();
        Collections.sort(regions);
        status = HttpStatus.OK;
        return new ResponseEntity<>(regions, status);
    }

    // Return given region by id
    @GetMapping("/{id}")
    public ResponseEntity<Region> getRegion(@PathVariable Long id) {
        Region region = new Region();
        // Check first if Character exists
        if (regionService.existsById(id)) {
            status = HttpStatus.OK;
            region = regionService.getRegionById(id);
        } else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(region, status);
    }

    // Create new region TODO - only admins should be able to do this
    // Check if this can be placed in KeycloakSecurityConfig file
    @RolesAllowed("admin")
    @PostMapping
    public ResponseEntity<Region> addRegion(@RequestBody Region region){
        Region returnRegion = regionService.save(region);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnRegion, status);
    }

    // Update region TODO - only admins should be able to do this
    @PutMapping("/{id}")
    public ResponseEntity<Region> updateRegion(@PathVariable long id, @RequestBody Region region){
        Region returnRegion = regionService.updateRegionById(id, region);
        status = HttpStatus.OK;
        if (returnRegion == null) status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(returnRegion, status);
    }

    // Method deletes a region
    @DeleteMapping("/{id}")
    ResponseEntity<Shipment> deleteRegion(@PathVariable long id) {
        // Check if region exists, then delete if it does
        if (regionService.existsById(id)) {
            status = HttpStatus.OK;
            regionService.deleteByID(id);
        }
        else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(status);
    }

}

