package com.noroff.boxinator.controllers;

import com.noroff.boxinator.models.Address;
import com.noroff.boxinator.models.Profile;
import com.noroff.boxinator.models.Shipment;
import com.noroff.boxinator.services.AddressService;
import com.noroff.boxinator.services.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@CrossOrigin(origins = "https://lit-spire-70256.herokuapp.com/")
@RequestMapping("/api/addresses")
public class AddressController {

    @Autowired
    AddressService addressService;

    private HttpStatus status;

    // Return all addresses
    @RolesAllowed("admin")
    @GetMapping()
    public ResponseEntity<List<Address>> getAddresses(){
        List<Address> addresses = addressService.getAddresses();
        status = HttpStatus.OK;
        return new ResponseEntity<>(addresses, status);
    }

    @PostMapping
    public ResponseEntity<Address> addAddress(@RequestBody Address address){
        Address returnAddress = addressService.save(address);
        status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnAddress, status);
    }




}
