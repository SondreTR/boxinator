package com.noroff.boxinator.services;

import com.noroff.boxinator.models.Address;
import com.noroff.boxinator.models.Package;
import com.noroff.boxinator.models.Status;
import com.noroff.boxinator.repositories.AddressRepository;
import com.noroff.boxinator.repositories.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatusService {

    @Autowired
    StatusRepository statusRepository;

    StatusService(){}

    public List<Status> getStatuses(){
        return statusRepository.findAll();
    }

    public Status save(Status status) { return statusRepository.save(status); }


}
