package com.noroff.boxinator.services;

import com.noroff.boxinator.models.Address;
import com.noroff.boxinator.models.Profile;
import com.noroff.boxinator.repositories.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService {

    @Autowired
    AddressRepository addressRepository;

    AddressService(){}

    public List<Address> getAddresses(){
        return addressRepository.findAll();
    }

    public Address save(Address address) {
        return addressRepository.save(address);
    }
}
