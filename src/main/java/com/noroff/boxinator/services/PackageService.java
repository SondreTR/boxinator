package com.noroff.boxinator.services;

import com.noroff.boxinator.models.Address;
import com.noroff.boxinator.models.Package;
import com.noroff.boxinator.models.Shipment;
import com.noroff.boxinator.repositories.PackageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class PackageService {

    @Autowired
    PackageRepository packageRepository;

    @Autowired
    ShipmentService shipmentService;

    @Autowired
    EmailService emailService;

    PackageService() {}

    public List<Package> getPackages(){
        return packageRepository.findAll();
    }

    public Package getPackageById(long id){
        return packageRepository.findById(id);
    }

    public boolean existsById(long id){
        return packageRepository.existsById(id);
    }

    public Package addPackageToShipment(long id, Package p){
        // Retrieve the shipment which shall own the package
        Shipment shipment = shipmentService.getShipmentById(id);
        // Set shipment as package's shipment
        p.setShipment(shipment);
        // Save the package in database
        Package returnPackage = this.save(p);
        // Add package to shipment's packages
        Set<Package> packages = shipment.getPackages();
        packages.add(returnPackage);
        // Add cost of package to shipment's total cost
        double packageCost = Integer.parseInt(p.getWeight()) * shipment.getAddress().getCountry().getRegion().getMultiplier();
        shipment.setCost(shipment.getCost() + packageCost);
        shipmentService.save(shipment);



        return p;
    }

    public Package save(Package p) { return packageRepository.save(p); }
}
