package com.noroff.boxinator.services;

import com.noroff.boxinator.models.Address;
import com.noroff.boxinator.models.Country;
import com.noroff.boxinator.models.Profile;
import com.noroff.boxinator.models.Region;
import com.noroff.boxinator.models.dtos.CountryDTO;
import com.noroff.boxinator.repositories.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryService {

    @Autowired
    CountryRepository countryRepository;

    @Autowired RegionService regionService;

    CountryService(){}

    public List<Country> getCountries(){ return countryRepository.findAll(); }

    public Country getCountryById(long id) { return countryRepository.findById(id); }

    public Country getCountryByName(String name) { return countryRepository.findByName(name); }

    public boolean existsByCountryName(String name) { return countryRepository.existsCountryByName(name); }

    public boolean existsById(long id) { return countryRepository.existsById(id); }

    public Country save(Country country) {
        return countryRepository.save(country);
    }

    public Country createCountryWithRegion(CountryDTO countryDTO) {

        Region region = null;

        if (regionService.existsByRegionName(countryDTO.getRegionName())) {
            region = regionService.getRegionByName(countryDTO.getRegionName());
        } else {
            return null;
        }

        Region returnRegion = regionService.save(region);

        Country country = new Country(
                countryDTO.getCountryName()
                //returnRegion
        );

        country.setRegion(returnRegion);

        return countryRepository.save(country);
    }

    public Country updateCountryById(long id, CountryDTO countryDTO) {
        if (countryRepository.findById(id) == null) return null;
        Country country = countryRepository.findById(id);
        country.setName(countryDTO.getCountryName());
        Region region = regionService.getRegionByName(countryDTO.getRegionName());
        country.setRegion(region);
        return countryRepository.save(country);
    }
}
