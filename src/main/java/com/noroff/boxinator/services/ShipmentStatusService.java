package com.noroff.boxinator.services;

import com.noroff.boxinator.models.Address;
import com.noroff.boxinator.models.ShipmentStatus;
import com.noroff.boxinator.repositories.AddressRepository;
import com.noroff.boxinator.repositories.ShipmentStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShipmentStatusService {

    @Autowired
    ShipmentStatusRepository shipmentStatusRepository;

    ShipmentStatusService(){}

    public List<ShipmentStatus> getShipmentStatuses(){
        return shipmentStatusRepository.findAll();
    }

    public ShipmentStatus save(ShipmentStatus shipmentStatus) {
        return shipmentStatusRepository.save(shipmentStatus);
    }
}
