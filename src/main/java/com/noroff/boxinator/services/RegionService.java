package com.noroff.boxinator.services;

import com.noroff.boxinator.models.*;
import com.noroff.boxinator.models.Package;
import com.noroff.boxinator.models.dtos.CountryDTO;
import com.noroff.boxinator.repositories.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegionService {

    @Autowired
    RegionRepository regionRepository;

    RegionService(){}

    public List<Region> getRegions(){
        return regionRepository.findAll();
    }

    public Region getRegionById(long id){
        return regionRepository.findById(id);
    }

    public boolean existsById(long id){
        return regionRepository.existsById(id);
    }

    public Region save(Region region) { return regionRepository.save(region); }

    public boolean existsByRegionName(String regionName) {
       return regionRepository.existsRegionByName(regionName);
    }

    public Region getRegionByName(String regionName) {
        return regionRepository.findByName(regionName);
    }

    public Region updateRegionById(long id, Region region) {
        if (regionRepository.findById(id) == null) return null;
        Region returnRegion = regionRepository.findById(id);
        returnRegion.setName(region.getName());
        returnRegion.setMultiplier(region.getMultiplier());
        return regionRepository.save(returnRegion);
    }

    public void deleteByID(long regionId) {
        Region region = regionRepository.findById(regionId);
        regionRepository.delete(region);
    }
}
