package com.noroff.boxinator.services;

import com.noroff.boxinator.models.*;
import com.noroff.boxinator.models.Package;
import com.noroff.boxinator.models.dtos.ShipmentDTO;
import com.noroff.boxinator.models.enums.AccountType;
import com.noroff.boxinator.models.enums.StatusString;
import com.noroff.boxinator.repositories.PackageRepository;
import com.noroff.boxinator.repositories.ShipmentRepository;
import com.noroff.boxinator.repositories.ShipmentStatusRepository;
import com.noroff.boxinator.repositories.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class ShipmentService {

    @Autowired
    ShipmentRepository shipmentRepository;

    @Autowired
    StatusService statusService;

    @Autowired
    ShipmentStatusRepository shipmentStatusRepository;

    @Autowired
    StatusRepository statusRepository;

    @Autowired
    PackageRepository packageRepository;

    @Autowired
    ProfileService profileService;

    @Autowired
    CountryService countryService;

    @Autowired
    AddressService addressService;

    @Autowired
    ShipmentStatusService shipmentStatusService;

    @Autowired
    PackageService packageService;

    @Autowired
    EmailService emailService;



    ShipmentService() {}

    // Format for date
    public DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public List<Shipment> getShipments(){
        List<Shipment> shipments = shipmentRepository.findAll();
        return shipments;
    }

    public Shipment getShipmentById(long id){
        Shipment shipment = shipmentRepository.findById(id);
        return shipment;
    }

    public boolean existsById(long id){
        return shipmentRepository.existsById(id);
    }

    public Shipment save(Shipment shipment) {
        return shipmentRepository.save(shipment);
    }

    public Shipment updateShipmentStatusById(long id, ShipmentDTO shipmentDTO) {
        // Return null if shipment is not found. This will return 404 NOT_FOUND
        if (shipmentRepository.findById(id) == null) return null;

        Shipment shipment = shipmentRepository.findById(id);

        // Get all shipmentStatuses from shipment
        Set<ShipmentStatus> shipmentStatuses = shipment.getShipmentStatuses();

        // Create new status with the status received from admin
        ShipmentStatus newShipmentStatus = new ShipmentStatus();
        Status newStatus = new Status(shipmentDTO.getStatus());
        newShipmentStatus.setStatus(newStatus);
        newShipmentStatus.setShipment(shipment);

        // Set date for shipmentStatus
        LocalDateTime date = LocalDateTime.now();

        newShipmentStatus.setDate(formatter.format(date));

        // Store in database
        statusRepository.save(newStatus);
        shipmentStatusRepository.save(newShipmentStatus);

        // Add new shipmentStatus to shipments' shipmentStatuses
        shipmentStatuses.add(newShipmentStatus);
        shipment.setShipmentStatuses(shipmentStatuses);
        return shipmentRepository.save(shipment);
    }

    public void deleteByID(long shipmentId) {
        Shipment shipment = shipmentRepository.findById(shipmentId);
        // Delete related packages
        for (Package pack: shipment.getPackages()) packageRepository.delete(pack);
        // Delete all related statuses and shipmentStatuses
        for (ShipmentStatus shipmentStatus: shipment.getShipmentStatuses()) {
            shipmentStatusRepository.delete(shipmentStatus);
            statusRepository.delete(shipmentStatus.getStatus());
        }
        shipmentRepository.delete(shipment);
    }

    // Create shipment for a registered user
    public Shipment createShipment(String keycloakId, ShipmentDTO shipmentDTO){

        Profile profile = profileService.findByKeycloakId(keycloakId);
        // Check first if profile exists and retrieve it from database
        if (profile == null) return null;

        Shipment shipment = createShipmentInDatabase(shipmentDTO, profile);

        // Add shipment to profile's shipments
        Set<Shipment> profileShipments = profile.getShipments();
        profileShipments.add(shipment);
        profile.setShipments(profileShipments);

        profileService.save(profile);
        return shipment;
    }



    // Create shipment for a guest
    public Shipment createGuestShipment(ShipmentDTO shipmentDTO) {
        // Guest profile shall only have email and account type
        Profile guest = new Profile();
        guest.setEmail(shipmentDTO.getEmail());
        guest.setAccountType(AccountType.GUEST);
        profileService.save(guest);
        guest = profileService.getProfileById(guest.getId());

        // Use helper method to create shipment in db
        Shipment shipment = createShipmentInDatabase(shipmentDTO, guest);

        // Add created shipment to guest's shipments
        Set<Shipment> profileShipments = new LinkedHashSet<>();
        profileShipments.add(shipment);
        guest.setShipments(profileShipments);
        profileService.save(guest);
        return shipment;
    }

    // Create a shipment for a given profile and create all related packages and statuses
    public Shipment createShipmentInDatabase(ShipmentDTO shipmentDTO, Profile profile){
        // Create new country or receive already existing country
        Country country = new Country(shipmentDTO.getCountry());

        if (countryService.existsByCountryName(shipmentDTO.getCountry())) {
            country = countryService.getCountryByName(shipmentDTO.getCountry());
        }

        // Create new address in database. This will create duplicates if identical addresses already exist.
        // This is up to us to decide, but Nick suggested creating new addresses for each
        Address receiverAddress = new Address(
                shipmentDTO.getStreet(),
                shipmentDTO.getCity(),
                shipmentDTO.getPostalCode(),
                shipmentDTO.getState(),
                country
        );
        addressService.save(receiverAddress);

        // Create new shipment and set the address, profile and packages in shipment's properties
        Shipment shipment = new Shipment(
                profile.getEmail()
        );
        shipment.setAddress(receiverAddress);

        shipment.setProfile(profile);

        // Save new shipment to database
        shipmentRepository.save(shipment);

        shipment.setCost(200);
        // Only add packages if they are provided because a shipment can be created without packages
        if (shipment.getPackages() != null){
            Set<Package> packages = new HashSet<>();
            // Loop over provided packages and create Java objects
            for (Package pack : shipmentDTO.getPackages()) {
                packages.add(pack);
                pack.setShipment(shipment);
                packageService.save(pack);
                // Add cost of package to total cost for shipment
                shipment.setCost(shipment.getCost() + Integer.parseInt(pack.getWeight()) * receiverAddress.getCountry().getRegion().getMultiplier());
            }
            // Set packages to the shipment
            shipment.setPackages(packages);
        }

        // Created shipment status and add status. Add them all to db.
        ShipmentStatus shipmentStatus = new ShipmentStatus();

        Status created = new Status(StatusString.CREATED);

        statusService.save(created);

        LocalDateTime date = LocalDateTime.now();

        shipmentStatus.setDate(formatter.format(date));

        shipmentStatus.setStatus(created);

        shipmentStatus.setShipment(shipment);

        shipmentStatusService.save(shipmentStatus);

        Set<ShipmentStatus> shipmentStatuses = new HashSet<>();

        shipmentStatuses.add(shipmentStatus);

        shipment.setShipmentStatuses(shipmentStatuses);
        return shipment;
    }

    public List<Shipment> getCompletedShipments(String keyCloakID) {
        Profile userProfile = profileService.findByKeycloakId(keyCloakID);
        List<Shipment> completedShipments = new ArrayList<>();
        Set<Shipment> shipments = userProfile.getShipments();
        // Find all shipments where the last status is COMPLETED
        for (Shipment shipment : shipments) {
            // Sort list by date from early to late
            ArrayList<ShipmentStatus> arrayList = new ArrayList<>();
            arrayList.addAll(shipment.getShipmentStatuses());
            Collections.sort(arrayList);
            // If last status is COMPLETED, add it to list to return
            if (arrayList.get(arrayList.size()-1).getStatus().getStatus() == StatusString.COMPLETED) completedShipments.add(shipment);
        }
        return completedShipments;
    }

    public List<Shipment> getCancelledShipments(String keyCloakID) {
        Profile userProfile = profileService.findByKeycloakId(keyCloakID);
        List<Shipment> completedShipments = new ArrayList<>();
        Set<Shipment> shipments = userProfile.getShipments();
        // Find all shipments where the last status is COMPLETED
        for (Shipment shipment : shipments) {
            // Sort list by date from early to late
            ArrayList<ShipmentStatus> arrayList = new ArrayList<>();
            arrayList.addAll(shipment.getShipmentStatuses());
            Collections.sort(arrayList);
            // If last status is COMPLETED, add it to list to return
            if (arrayList.get(arrayList.size()-1).getStatus().getStatus() == StatusString.CANCELLED) completedShipments.add(shipment);
        }
        return completedShipments;
    }

    public Shipment getShipmentReceipt(Long id) {

        Shipment shipment = this.getShipmentById(id);
        Set<Package> packages = shipment.getPackages();

        String packagesString = "Packages ";
        for (Package pack : packages){
            String string = "";
            string += "\nType: " + pack.getType();
            string += "\nWeight: " + pack.getWeight() + "kg";
            packagesString += string.indent(5);
        }

        String addressString =
                "\nStreet: " + shipment.getAddress().getStreet()
                + "\nCity: " + shipment.getAddress().getCity()
                + "\nPostal code: " + shipment.getAddress().getPostalCode()
                + "\nState: " + shipment.getAddress().getState()
                + "\nCountry: " + shipment.getAddress().getCountry().getName();

        String email = "Here's you receipt: " +
                "\n" + packagesString +
                "\nCost: " + shipment.getCost() + "kr" +
                "\n\nAddress: " + addressString;

        // Send email to guests with registration link
        if (shipment.getProfile().getFirstName() == null){
            email += "\n\nLINK TO REGISTER: \nhttps://boxinator-keycloaks.herokuapp.com/auth/realms/boxinator/protocol/openid-connect/registrations?client_id=boxinator-client&redirect_uri=http%3A%2F%2Flocalhost%3A4200%2Fhome&state=317f8ad9-35c4-4190-8118-62a97addb213&response_mode=fragment&response_type=code&scope=openid&nonce=b3a68868-2718-46de-87ed-68dbbfd9c1dd";
        }

        // Send email
        emailService.sendEmail(shipment.getEmail(), "Receipt", email);

        return shipment;
    }
}

