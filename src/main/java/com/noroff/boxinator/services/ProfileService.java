package com.noroff.boxinator.services;

import com.noroff.boxinator.models.*;
import com.noroff.boxinator.models.dtos.ProfileDTO;
import com.noroff.boxinator.models.enums.AccountType;
import com.noroff.boxinator.repositories.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ProfileService {

    @Autowired
    ProfileRepository profileRepository;

    @Autowired
    CountryService countryService;

    @Autowired
    AddressService addressService;

    @Autowired
    ShipmentService shipmentService;

    ProfileService() {}

    public List<Profile> getProfiles() {
        List<Profile> profiles = profileRepository.findAll();
        for (Profile p : profiles){
            // Sort shipments by date created
            ArrayList<Shipment> list = new ArrayList<>(p.getShipments());
            Collections.sort(list);
            p.setShipments(new LinkedHashSet<>(list));
        }
        return profiles;
    }

    public Profile getProfileById(long id) {
        Profile profile = profileRepository.findById(id);

        // Sort shipments by date created
        ArrayList<Shipment> list = new ArrayList<>(profile.getShipments());
        Collections.sort(list);
        profile.setShipments(new LinkedHashSet<>(list));
        return profile;
    }

    public boolean existsById(long id) {
        return profileRepository.existsById(id);
    }

    public Profile findByKeycloakId(String keycloakId) {
        Profile profile = profileRepository.findByKeycloakId(keycloakId);
        // Sort shipments by date created
        ArrayList<Shipment> list = new ArrayList<>(profile.getShipments());
        Collections.sort(list);
        profile.setShipments(new LinkedHashSet<>(list));
        return profile;
    }

    public boolean existsByEmail(String email) { return profileRepository.existsByEmail(email); }

    public Profile save(Profile profile) {
        Profile p = profileRepository.save(profile);
        // Sort shipments by date created
        ArrayList<Shipment> list = new ArrayList<>(profile.getShipments());
        Collections.sort(list);
        profile.setShipments(new LinkedHashSet<>(list));
        return p;
    }

    public Profile createProfileWithAddress(ProfileDTO profileDTO) {

        if (this.existsByEmail(profileDTO.getEmail())) return null;

        // Create new country or retrieve existing country
        Country country = new Country(profileDTO.getCountry());

        if (countryService.existsByCountryName(profileDTO.getCountry())) {
            country = countryService.getCountryByName(profileDTO.getCountry());
        }

        // Create new address
        Address address = new Address(
                profileDTO.getStreet(),
                profileDTO.getCity(),
                profileDTO.getPostalCode(),
                profileDTO.getState(),
                country
        );

        Address returnAddress = addressService.save(address);

        // Create new profile
        Profile profile = new Profile(
                profileDTO.getFirstName(),
                profileDTO.getLastName(),
                profileDTO.getEmail(),
                profileDTO.getDateOfBirth(),
                profileDTO.getContactNumber()
        );
        profile.setAccountType(AccountType.REGISTERED_USER);
        profile.setAddress(returnAddress);
        profile.setKeycloakId(profileDTO.getKeycloakId());
        // Sort shipments by date created
        ArrayList<Shipment> list = new ArrayList<>(profile.getShipments());
        Collections.sort(list);
        profile.setShipments(new LinkedHashSet<>(list));
        return profileRepository.save(profile);
    }

    public void deleteByID(long id) {
        Profile profile = profileRepository.findById(id);
        // Delete all related shipments, packages, statuses
        for (Shipment s : profile.getShipments()) shipmentService.deleteByID(s.getId());
        profileRepository.delete(profile);
    }

    public Profile updateProfile(long id, ProfileDTO dto) {
        if (!profileRepository.existsById(id)) return null;

        Profile profile = profileRepository.findById(id);

        profile.setFirstName(dto.getFirstName());
        profile.setLastName(dto.getLastName());
        profile.setEmail(dto.getEmail());
        profile.setDateOfBirth(dto.getDateOfBirth());
        profile.setContactNumber(dto.getContactNumber());
        profile.setAccountType(dto.getAccountType());
        profile.setKeycloakId(dto.getKeycloakId());

        Country country = countryService.getCountryByName(dto.getCountry());
        if (country == null) return null;

        // Create new address if profile has no address registered
        if (profile.getAddress() == null) {
            Address address = new Address(
                    dto.getStreet(),
                    dto.getCity(),
                    dto.getPostalCode(),
                    dto.getState(),
                    country
            );
            addressService.save(address);
            profile.setAddress(address);
        }
        // Modify already existing address
        else {
            profile.getAddress().setStreet(dto.getStreet());
            profile.getAddress().setCity(dto.getCity());
            profile.getAddress().setPostalCode(dto.getPostalCode());
            profile.getAddress().setState(dto.getState());
            profile.getAddress().setCountry(country);
        }
        ArrayList<Shipment> list = new ArrayList<>(profile.getShipments());
        Collections.sort(list);
        profile.setShipments(new LinkedHashSet<>(list));
        return profileRepository.save(profile);
    }

    public Profile findProfileByEmail(String email) {
        if (existsByEmail(email)) {
            Profile profile = profileRepository.findByEmail(email);
            ArrayList<Shipment> list = new ArrayList<>(profile.getShipments());
            Collections.sort(list);
            profile.setShipments(new LinkedHashSet<>(list));
            return profile;
        } else return null;
    }
}
