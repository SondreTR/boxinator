package com.noroff.boxinator.config;

import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(jsr250Enabled = true)
public class KeycloakSecurityConfig extends KeycloakWebSecurityConfigurerAdapter {


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http.cors().and().csrf().disable().authorizeRequests()
                //Users and Admins can get list of all countries
                .antMatchers(HttpMethod.GET, "/api/settings/countries").hasAnyRole("user", "admin")
                //The rest of the methods in this endpoint is restricted to only admin
                .antMatchers("/api/settings/**").hasRole("admin")
                //Users and Admins can post new address, but only admins can get list of addresses
                .antMatchers(HttpMethod.POST, "/api/addresses").hasAnyRole("user", "admin")
                //Users and Admins can create new profiles
                .antMatchers(HttpMethod.POST, "/api/profiles").hasAnyRole("user", "admin")
                //Put method should be allowed users as well
                .antMatchers(HttpMethod.PUT, "/api/profiles/{id}").hasAnyRole("user", "admin")
                //The rest of the methods in this endpoint is restricted to only admin
                .antMatchers("/api/profiles/**").hasRole("admin")
                //Admin only has access to regions
                .antMatchers("/api/regions/**").hasRole("admin")
                //Admin and user can create shipments
                .antMatchers(HttpMethod.POST, "/api/shipments/*").hasAnyRole("user", "admin")
                .antMatchers("/api/shipments/**").hasRole("admin")
                .antMatchers("/api/shipmentstatuses").hasRole("admin")
                .antMatchers("/api/statuses").hasRole("admin")
                .anyRequest()
                .permitAll();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/api/**").allowedOrigins("*").allowedMethods("*");
            }
        };
    }





    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();

        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(new SimpleAuthorityMapper());
            auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
    }

    @Bean
    public KeycloakConfigResolver KeycloakConfigResolver(){
        return new KeycloakSpringBootConfigResolver();
    }
}
