package com.noroff.boxinator;


import com.noroff.boxinator.models.Country;
import com.noroff.boxinator.models.Region;
import com.noroff.boxinator.repositories.CountryRepository;
import com.noroff.boxinator.repositories.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class Seeder implements ApplicationRunner {

    @Autowired
    CountryRepository countryRepository;

    @Autowired
    RegionRepository regionRepository;

    public void seeder(){
        // Create default regions
        Region scandinavia = new Region("Scandinavia", 1);
        Region eu = new Region("EU", 5);
        Region restOfEurope = new Region("Rest of Europe", 8);
        Region mena = new Region("Middle East and North Africa", 20);
        Region southAsia = new Region("South Asia", 30);
        Region eastAsia = new Region("East Asia", 25);
        Region southEastAsia = new Region("South East Asia", 35);
        Region africa = new Region("Africa", 50);
        Region northAmerica = new Region("North America", 15);
        Region latinAmericaAndTheCaribbean = new Region("Latin America and the Caribbean", 30);
        Region oceania = new Region("Oceania", 70);

        // Saving all regions to db
        for (Region region : Arrays.asList(scandinavia, eu, restOfEurope, mena, southAsia, eastAsia, southEastAsia, latinAmericaAndTheCaribbean, northAmerica, oceania, africa)){
            regionRepository.save(region);
        }

        // Create all default countries

        // Scandinavia
        Set<Country> scandinavianCountries = new HashSet<>();
        Country norway = new Country("Norway");
        Country sweden = new Country("Sweden");
        Country denmark = new Country("Denmark");

        /* Setting Scandinavian countries to their region */
        for (Country country : Arrays.asList(norway, sweden, denmark)) {
            country.setRegion(scandinavia);
            countryRepository.save(country);
        }

        /* EU and EEC */
        Country finland = new Country ("Finland");
        Country iceland = new Country("Iceland");
        Country faroe = new Country("Faroe Islands");
        Country spain = new Country("Spain");
        Country france = new Country("France");
        Country germany = new Country("Germany");
        Country italy = new Country("Italy");
        Country portugal = new Country("Portugal");
        Country switzerland = new Country("Switzerland");
        Country austria = new Country("Austria");
        Country netherlands = new Country("The Netherlands");
        Country belgium = new Country("Belgium");
        Country greece = new Country("Greece");
        Country latvia = new Country("Latvia");
        Country lithuania = new Country("Lithuania");
        Country ireland = new Country("Ireland");
        Country luxembourg = new Country("Luxembourg");

        /* Setting EU countries to their region */
        for (Country country : Arrays.asList(finland, iceland, faroe, spain, germany, france, italy,
                portugal, belgium, switzerland, austria, netherlands, greece, latvia, lithuania, ireland, luxembourg)) {
            country.setRegion(eu);
            countryRepository.save(country);
        }

        /* Rest of Europe */
        Country serbia = new Country("Serbia");
        Country russia = new Country("Russia");
        Country bosnia = new Country("Bosnia & Herzegovina");
        Country montenegro = new Country("Montenegro");
        Country albania = new Country("Albania");
        Country georgia = new Country("Georgia");
        Country ukraine = new Country("Ukraine");
        Country turkey = new Country("Turkey");
        Country belarus = new Country("Belarus");
        Country moldova = new Country("Moldova");
        Country uk = new Country("United Kingdom");

        /* Setting remaining European countries to their region */
        for (Country country : Arrays.asList(serbia, russia, bosnia, montenegro, moldova, albania, georgia, ukraine, turkey, belarus, uk)) {
            country.setRegion(restOfEurope);
            countryRepository.save(country);
        }

        /* East Asia */
        Country japan = new Country("Japan");
        Country skorea = new Country("South Korea");
        Country nkorea = new Country("North Korea");
        Country taiwan = new Country("Taiwan");
        Country china = new Country("China");
        Country mongolia = new Country("Mongolia");

        /* Setting East Asian countries to their region */
        for (Country country : Arrays.asList(china, japan, skorea, nkorea, taiwan, mongolia)) {
            country.setRegion(eastAsia);
            countryRepository.save(country);
        }

        /* South Asia */
        Country india = new Country("India");
        Country pakistan = new Country("Pakistan");
        Country srilanka = new Country("Sri Lanka");
        Country bangladesh = new Country("Bangladesh");
        Country nepal = new Country("Nepal");

        /* Setting South Asian countries to their region */
        for (Country country : Arrays.asList(india, pakistan, srilanka, bangladesh, nepal )) {
            country.setRegion(southAsia);
            countryRepository.save(country);
        }

        /* South East Asia */
        Country thailand = new Country("Thailand");
        Country singapore = new Country("Singapore");
        Country vietnam = new Country("Vietnam");
        Country malaysia = new Country("Malaysia");
        Country indonesia = new Country("Indonesia");

        /* Setting South East Asian countries to their region */
        for (Country country : Arrays.asList(thailand, malaysia, indonesia, singapore, vietnam)) {
            country.setRegion(southEastAsia);
            countryRepository.save(country);
        }

        /* Middle East and North Africa */
        Country egypt = new Country("Egypt");
        Country uae = new Country("United Arab Emirates");
        Country israel = new Country("Israel");
        Country qatar = new Country("Qatar");
        Country morocco = new Country("Morocco");
        Country algeria = new Country("Algeria");

        /* Setting mena countries to their region */
        for (Country country : Arrays.asList(egypt, uae, israel, qatar, morocco, algeria)) {
            country.setRegion(mena);
            countryRepository.save(country);
        }

        /* Latin America and the Caribbean */
        Country brazil = new Country("Brazil");
        Country argentina = new Country("Argentina");
        Country chile = new Country("Chile");
        Country jamaica = new Country("Jamaica");
        Country stkittsneves = new Country("St. Kitts & Neves");
        Country trinidad = new Country("Trinidad & Tobago");

        /* Setting Latin American countries to their region */
        for (Country country : Arrays.asList(argentina, brazil, chile, jamaica, stkittsneves, trinidad)) {
            country.setRegion(latinAmericaAndTheCaribbean);
            countryRepository.save(country);
        }

        /* North America */
        Country usa = new Country("USA");
        Country canada = new Country("Canada");

        /* Setting North American countries to their region */
        for (Country country : Arrays.asList(usa, canada)) {
            country.setRegion(northAmerica);
            countryRepository.save(country);
        }

        /* Oceania */
        Country australia = new Country("Australia");
        Country newZealand = new Country("New Zealand");
        Country samoa = new Country("Samoa");

        /* Setting region for oceanian countries */
        for (Country country : Arrays.asList(australia, newZealand, samoa)) {
            country.setRegion(oceania);
            countryRepository.save(country);
        }

        /* Africa */
        Country somalia = new Country("Somalia");
        Country southAfrica = new Country("South Africa");
        Country nigeria = new Country("Nigeria");
        Country ghana = new Country("Ghana");

        /* Setting region for African countries */
        for (Country country : Arrays.asList(somalia, southAfrica, nigeria, ghana)) {
            country.setRegion(africa);
            countryRepository.save(country);
        }
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (regionRepository.count() == 0 && countryRepository.count() == 0)
            seeder();
    }
}
