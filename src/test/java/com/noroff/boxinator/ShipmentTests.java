package com.noroff.boxinator;

import com.noroff.boxinator.models.Package;
import com.noroff.boxinator.models.Profile;
import com.noroff.boxinator.models.Shipment;
import com.noroff.boxinator.models.ShipmentStatus;
import com.noroff.boxinator.models.dtos.ShipmentDTO;
import com.noroff.boxinator.models.enums.AccountType;
import com.noroff.boxinator.models.enums.StatusString;
import com.noroff.boxinator.services.ProfileService;
import com.noroff.boxinator.services.ShipmentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class ShipmentTests {

    @Autowired
    ProfileService profileService;

    @Autowired
    ShipmentService shipmentService;

    public String improvedRandomString() {
        // create a string of all characters
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        // create random string builder
        StringBuilder sb = new StringBuilder();

        // create an object of Random class
        Random random = new Random();

        // specify length of random string
        int length = 7;

        for(int i = 0; i < length; i++) {

            // generate random index number
            int index = random.nextInt(alphabet.length());

            // get character specified by index
            // from the string
            char randomChar = alphabet.charAt(index);

            // append the character to string builder
            sb.append(randomChar);
        }

        String randomString = sb.toString();
        System.out.println("Random String is: " + randomString);
        return randomString.toLowerCase();
    }

    @Test
    void createNewShipment_checkIfShipmentIsCreated_shouldReturnShipment(){
        // Arrange
        Profile profile = new Profile();
        profile.setEmail(improvedRandomString() + "@gmail.com");
        profile.setAccountType(AccountType.REGISTERED_USER);
        profileService.save(profile);

        ShipmentDTO shipmentDTO = new ShipmentDTO(
                profile.getEmail(),
                "calle 456",
                "Madrid",
                "4829",
                "Comunidad de Madrid",
                "Spain"
        );

        Set<Package> packages = new HashSet<>();
        packages.add(
                new Package(
                        "deluxe",
                        "10kg",
                        "purple"
                )
        );

        //Act
        shipmentDTO.setPackages(packages);
        Shipment shipment = shipmentService.createShipment(profile.getKeycloakId(), shipmentDTO);

        //Assert
        assertEquals(shipmentDTO.getCountry(), shipment.getAddress().getCountry().getName());
        assertEquals(shipmentDTO.getCity(), shipment.getAddress().getCity());
        assertEquals(shipmentDTO.getStreet(), shipment.getAddress().getStreet());
        assertEquals(shipmentDTO.getState(), shipment.getAddress().getState());
        assertEquals(shipmentDTO.getPostalCode(), shipment.getAddress().getPostalCode());
        assertEquals(shipmentDTO.getEmail(), shipment.getEmail());
        assertEquals(profile.getEmail(), shipment.getProfile().getEmail());
        // Convert set of shimpentStatuses to array
        ShipmentStatus[] ss = shipment.getShipmentStatuses().toArray(new ShipmentStatus[shipment.getShipmentStatuses().size()]);
        assertEquals(StatusString.CREATED, ss[0].getStatus().getStatus());
        assertEquals(shipmentDTO.getPackages(), shipment.getPackages());
    }

  /*  @Test
   void createNewGuestShipment_checkIfGuestShipmentIsCreated_shouldReturnShipment(){
        // Arrange
        ShipmentDTO shipmentDTO = new ShipmentDTO(
                improvedRandomString() + "@gmail.com",
                "calle 456",
                "Madrid",
                "4829",
                "Comunidad de Madrid",
                "Spain"
        );

        Set<Package> packages = new HashSet<>();
        packages.add(
                new Package(
                        "humble",
                        "2kg",
                        "yellow"
                )
        );

        //Act
        shipmentDTO.setPackages(packages);
        Shipment shipment = shipmentService.createGuestShipment(shipmentDTO);

        //Assert
        assertEquals(shipmentDTO.getCountry(), shipment.getAddress().getCountry().getName());
        assertEquals(shipmentDTO.getCity(), shipment.getAddress().getCity());
        assertEquals(shipmentDTO.getStreet(), shipment.getAddress().getStreet());
        assertEquals(shipmentDTO.getState(), shipment.getAddress().getState());
        assertEquals(shipmentDTO.getPostalCode(), shipment.getAddress().getPostalCode());
        assertEquals(shipmentDTO.getEmail(), shipment.getEmail());
        // Convert set of shimpentStatuses to array
        ShipmentStatus[] ss = shipment.getShipmentStatuses().toArray(new ShipmentStatus[shipment.getShipmentStatuses().size()]);
        assertEquals(StatusString.CREATED, ss[0].getStatus().getStatus());
        assertEquals(shipmentDTO.getPackages(), shipment.getPackages());
    }*/

    @Test
    void calculateCost_checkShipmentCostSpain_expectedEUCost() {
        //Arrange
        Profile profile = new Profile();
        profile.setEmail(improvedRandomString() + "@gmail.com");
        profileService.save(profile);

        ShipmentDTO shipmentDTO = new ShipmentDTO(
                profile.getEmail(),
                "calle 456",
                "Madrid",
                "4829",
                "Comunidad de Madrid",
                "Spain"
        );

        Set<Package> packages = new HashSet<>();
        packages.add(
                new Package(
                        "deluxe",
                        "5kg",
                        "purple"
                )
        );
        packages.add(
                new Package(
                        "humble",
                        "2kg",
                        "red"
                )
        );

        int expectedCost = 200 + (5 * 5) + (2 * 5);

        //Act
        shipmentDTO.setPackages(packages);
        Shipment shipment = shipmentService.createShipment(profile.getKeycloakId(), shipmentDTO);
        double multiplier = shipment.getAddress().getCountry().getRegion().getMultiplier();
        int totalCost = 200;

        Package[] pw = shipment.getPackages().toArray(new Package[shipment.getPackages().size()]);
        int[] weights = new int[pw.length];
        for (int i = 0; i < pw.length; i++) {
            weights[i] = Integer.parseInt((pw[i].getWeight()).replace("kg", ""));
            totalCost += multiplier * weights[i];
        }

        //Assert
        assertEquals(expectedCost , totalCost);
    }

    @Test
    void calculateCost_checkShipmentCostJapan_expectedEastAsiaCost() {
        //Arrange
        Profile profile = new Profile();
        profile.setEmail(improvedRandomString() + "@gmail.com");
        profileService.save(profile);

        ShipmentDTO shipmentDTO = new ShipmentDTO(
                profile.getEmail(),
                "Konichiwa 456",
                "Osaka",
                "0177",
                "Osaka",
                "Japan"
        );

        Set<Package> packages = new HashSet<>();
        packages.add(
                new Package(
                        "deluxe",
                        "5kg",
                        "purple"
                )
        );
        packages.add(
                new Package(
                        "basic",
                        "1kg",
                        "yellow"
                )
        );
        packages.add(
                new Package(
                        "premium",
                        "8kg",
                        "pink"
                )
        );

        int expectedCost = 200 + (5 * 25) + (1 * 25) + (8 * 25);

        //Act
        shipmentDTO.setPackages(packages);
        Shipment shipment = shipmentService.createShipment(profile.getKeycloakId(), shipmentDTO);
        double multiplier = shipment.getAddress().getCountry().getRegion().getMultiplier();
        int totalCost = 200;

        Package[] pw = shipment.getPackages().toArray(new Package[shipment.getPackages().size()]);
        int[] weights = new int[pw.length];
        for (int i = 0; i < pw.length; i++) {
            weights[i] = Integer.parseInt((pw[i].getWeight()).replace("kg", ""));
            totalCost += multiplier * weights[i];
        }

        //Assert
        assertEquals(expectedCost , totalCost);
    }
}
