package com.noroff.boxinator;

import com.noroff.boxinator.models.Package;
import com.noroff.boxinator.models.Profile;
import com.noroff.boxinator.models.Shipment;
import com.noroff.boxinator.models.ShipmentStatus;
import com.noroff.boxinator.models.dtos.ProfileDTO;
import com.noroff.boxinator.models.dtos.ShipmentDTO;
import com.noroff.boxinator.models.enums.AccountType;
import com.noroff.boxinator.models.enums.StatusString;
import com.noroff.boxinator.services.ProfileService;
import com.noroff.boxinator.services.ShipmentService;
import org.junit.jupiter.api.Test;
import org.postgresql.shaded.com.ongres.scram.common.bouncycastle.pbkdf2.Pack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;



@SpringBootTest
public class ProfileTests {

        @Autowired
        ProfileService profileService;

        @Autowired
        ShipmentService shipmentService;

        @Test
        void createNewProfile_checkIfUserIsCreated_ShouldReturnUser() {
                // Arrange
                ProfileDTO profileDTO = new ProfileDTO(
                        "Carl",
                        "Larsson",
                        "carllarsson@gmail.com",
                        "15.10.1987",
                        "94617451",
                        "Carlsgatan",
                        "Stockholm",
                        "7181",
                        "Stockholm",
                        "Sweden",
                        AccountType.REGISTERED_USER,
                        "1"
                );

                // Act
                Profile profile = profileService.createProfileWithAddress(profileDTO);

                // Assert
                assertEquals(profileDTO.getFirstName(), profile.getFirstName());
                assertEquals(profileDTO.getLastName(), profile.getLastName());
                assertEquals(profileDTO.getEmail(), profile.getEmail());
                assertEquals(profileDTO.getDateOfBirth(), profile.getDateOfBirth());
                assertEquals(profileDTO.getContactNumber(), profile.getContactNumber());
                assertEquals(profileDTO.getStreet(), profile.getAddress().getStreet());
                assertEquals(profileDTO.getCity(), profile.getAddress().getCity());
                assertEquals(profileDTO.getPostalCode(), profile.getAddress().getPostalCode());
                assertEquals(profileDTO.getState(), profile.getAddress().getState());
                assertEquals(profileDTO.getCountry(), profile.getAddress().getCountry().getName());
        }

        @Test
        void updateProfile_checkIfProfileIsUpdated_ShouldReturnUpdatedProfile() {
                // Arrange
                Profile profile = new Profile();
                profile.setEmail("testygs@email.com");
                profile.setAccountType(AccountType.GUEST);
                profileService.save(profile);


                ProfileDTO profileDTO = new ProfileDTO(
                        "Vladmimir",
                        "Putin",
                        "putin@gmail.com",
                        "19.06.1955",
                        "143565141",
                        "Cagdilat 123",
                        "Moscow",
                        "666",
                        "Moscow",
                        "Russia",
                        AccountType.REGISTERED_USER,
                        "0"
                );

                // Act
                Profile updateProfile = profileService.updateProfile(profile.getId(), profileDTO);

                // Assert
                assertEquals(profileDTO.getFirstName(), updateProfile.getFirstName());
                assertEquals(profileDTO.getLastName(), updateProfile.getLastName());
                assertEquals(profileDTO.getEmail(), updateProfile.getEmail());
                assertEquals(profileDTO.getDateOfBirth(), updateProfile.getDateOfBirth());
                assertEquals(profileDTO.getContactNumber(), updateProfile.getContactNumber());
                assertEquals(profileDTO.getStreet(), updateProfile.getAddress().getStreet());
                assertEquals(profileDTO.getCity(), updateProfile.getAddress().getCity());
                assertEquals(profileDTO.getPostalCode(), updateProfile.getAddress().getPostalCode());
                assertEquals(profileDTO.getState(), updateProfile.getAddress().getState());
                assertEquals(profileDTO.getCountry(), updateProfile.getAddress().getCountry().getName());
        }


}
