How to run locally
- clone from gitlab.
- Create postgres database named: BoxinatorDB
- Change password in application.properties to your postgress password.
- run build



CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended modules (?)
* Installation (?)
* Configuration
* Troubleshooting (?)
* FAQ (?)
* Maintainers 


INTRODUCTION
------------

The primary purpose of the software is to be a capstone experience for the candidates; they are expected to utilize a 
selection of possible development options to produce a single software solution that demonstrates their capabilities as 
developers. The candidates must produce a software solution that is considered a final product. The software is to be 
produced over a period of Four weeks.



Specifications:

This case describes an application designed for calculating the shipping cost for mystery boxes to specific locations 
around the world. It's a web application, using a RESTfulAPI to communicate with a server, and Angular as a front-end platform. 
Keycloack is used as authentication. 

There are 3 types of users/Account Types of this application: 
- Registered_user (ordinary customer)
- Adminstrator
- Guest-user (non-register user)

The boxes comes in 5 different types: 
- Basic tier, 1kg
- humble tier, 2kg
- deluxe tier, 5kg
- premium tier, 8kg

Users/Customers 
A customer with a registered user will be able to log in to their account. If they dont have an account, they will be able to create an account.
After they have logged in, they will be able to see the overview of all of their current shipments, as well as 
creating a new shipment. They will also be able to cancel a shipment. Every box has a color. As the customer add boxes to the shipment, 
they also chooses the colors of the specific boxes. The shipment`s cost is calculated by the shipment destination. The multiplier is decided
by the administrator. Every shipment has a starting "main" cost of 200kr. 

Guest-user 
A guest-user is a user without an account. When making a shipment, the guest-user

Administrator
An administrator will use the same log-in page as the customer-user. The adminstrator is able to change the shipment status of every shipment.
The administrator 

Technology
Angular 

Views
- login-page
This is a simple page where borh the administrator and customer can log in
- 
- Homepage 
-An administrator has access to a portal to change the metadata of the shipping process, this being the countries that can be shipped to and the relative costs of shipping.
-The candidates are expected to write unit/component tests for both the back-end and front-end. The packages to do this will differ depending on the frameworks used.
-The system comprises of the following components:
-A web front-end for customers
-A web front-end for administrators
-An API that receives data from the front-end and communicates with the database
-A relational database
-Front-end requirements:
-Using modern design concepts/frameworks, the system should be functional on both desktop computers and mobile devices.
-The front-end should be built using one of the following modern JavaScript frameworks:
-React
-Vue
-Angular
-Endpoints
-Login page
-This is a very simple login page where both users and administrators login.
-Users and administrators login with an email and password combination to authenticate themselves.
-There should be an option to register as a new user visible on the login screen.
-Register Page:
-If a user selects the option to register themselves they should be presented with a form to populate with the following information:
-First name (required)
-Last name (required)
-Email (required, validate on client side)
-Password (required, strong and must be confirmed by repetition)
-Date of birth
-Country of residence
-Zip code/postal code
-Contact number
-All fields should contain validation to ensure they are in the correct format based on the descriptions stated above.
-The system must dynamically indicate any invalid entries.
-A confirmation e-mail should be sent before an account becomes activated.
-The user can register as residing in any country, but Boxinator services use Norway, Sweden, Denmark as source countries.
-There should be a flat (standard) fee of 200 Kr for packages sent between these countries.
-It should cost the same to send a parcel from Stockholm (Sweden) to Oslo (Norway) as it would to send a parcel from Copenhagen (Denmark) to Oslo (Norway).
-For countries outside of the listed ones, an additional fee is added. These additional fees are calculated based on the package weight and a country multiplier, this is detailed below.
-Main page:
-Once a user has logged in, they are presented with a simple page which shows any shipments they have under way and recently completed shipments.
-There should be an option to add a new shipment.
-When a new shipment is added a modal should be displayed with the following fields:
-Receiver name
-Weight options
-Box colour
-Destination
-The boxes can be coloured to match any rgba value.
-The weight option is to select which tier of mystery box is being sent. This results in various predefined weights for the various options.
-The basic tier is a 1Kg box, the humble tier is a 2Kg box, the deluxe tier is a 5Kg box, and the premium tier is a 8Kg box.
-The user can then submit this and it is saved to the database. All fields should be validated in ways that are intuitive (e.g. valid RGB(A) values).
-The view of the finished and CREATED shipments are displayed in a table, ordered by date. Which each field captured being a column in the table, with the colour column being the actual colour and not the RGB values.
-The cost is shown underneath and will be stored in the database (for historical accounting purposes), which is the weigh in kg multiplied by the country multiplier in addition to the 200Kr flat fee, resulting in the following formula:
-‘Total cost = Flat fee + (weight*multiplier)‘. For example, shipping a deluxe package to Germany (with a multiplier of 5):
-Total cost = 200 (5*5) = 225Kr.
-The country multiplier is up to the group to decide, but it is based on distance from Oslo - use Germany with a multiplier of 5 as inspiration.
-Anonymous/guset usage
-A guest user must be able to send a shipment by filling out the needed delivery information and providing an e-mail only for receipt purposes (this must be clearly communicated to the user), they will be stored as guest role in the database, only having their email entered. This can be presented as a ”register later” option when a user that is not logged in is trying to create a shipment.
-The receipt e-mail must also contain a link that the user can use to create an account which will then also ‘claim’ the shipment.
-Administrator main page
-Once an administrator has logged in they are able to view all current shipments and their respective status’. From here the administrator has the ability to change a shipment’s state. Administrators may also update the country multipliers to affect the pricing of shipments.
-User account management
-There should be a page that can display the user’s account information. The user should also be able to update their account information.
-A guest user must not have access to an account page. They will need to create an account via the email sent when ordering a shipment.
-Package status
-This solution is for demonstration purposes, so all packages will move between states when triggered by the Administrator. There should be a debug page that allows this to be done easily.
-CREATED: This is the ‘new‘ package state
-RECIEVED: This represents when the package has been physically collected for transport, but has not been routed for delivery yet.
-INTRANSIT: Represents a package that is being transported.
-COMPLETED: Represents a package that has been delivered.
-CANCELLED: Represents a package that has been cancelled
-The shipment statuses must be recorded for history. This means that a user is able to view the complete history of the changes in a shipments status. A hint for this is to have many ‘shipment status‘ records for one ‘shipment‘. When a user sees a list of their shipments, they see the latest status for that shipment - keep in mind complete shipments should be shown separately.





USER GUIDE 
------------

The requirements section (required) shall make it clear whether this project requires anything outside of Drupal core to
work (modules, libraries, etc). List all requirements here, including those that follow indirectly from another module,
etc. The idea is to inform the users about what is required, so that everything they need can be procured and included
in advance of attempting to install the module. If there are no requirements, write "No special requirements".


RECOMMENDED MODULES
-------------------
This optional section lists modules that are not required, but that may enhance the usefulness or user experience of 
your project. Make sure to describe the benefits of enabling these modules.


CONFIGURATION
-------------
The configuration section (required) is necessary even when little configuration is required. Use this section to list 
special notes about the configuration of this module – including but not limited to permissions. This section is 
particularly important if the module requires additional configuration outside of the Drupal UI.

If the module has little or no configuration, you should use this space to explain how enabling/disabling the module 
will affect the site.
