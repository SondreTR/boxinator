FROM openjdk:17
ADD target/boxinator-0.0.1-SNAPSHOT.jar boxinator.jar
ENTRYPOINT ["java", "-jar","boxinator.jar"]